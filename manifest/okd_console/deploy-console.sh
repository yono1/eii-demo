#!/bin/bash

oc create serviceaccount console -n kube-system
oc create clusterrolebinding console --clusterrole=cluster-admin --serviceaccount=kube-system:console -n kube-system
sa=`oc get sa console -o jsonpath='{.imagePullSecrets[0].name}' -n kube-system`
tokenname=`oc get secret $sa -n kube-system -o jsonpath='{.metadata.ownerReferences[0].name}'`
sed -i "s/name: CHANGEME/name: $tokenname/g" console.yaml 
oc apply -f console.yaml
oc get pods -n kube-system
