import os
import sys
import cv2
import time
from kafka import KafkaProducer

topic = "video"
KAFKA_SERVER = os.getenv('KAFKA_SERVER')
if KAFKA_SERVER:
    KAFKA_SERVER = KAFKA_SERVER + '9092'
elif not KAFKA_SERVER:
    KAFKA_SERVER = 'localhost:9092'


def publish_camera():
    # Start up producer
    producer = KafkaProducer(bootstrap_servers=KAFKA_SERVER)
    camera = cv2.VideoCapture(-1)
    camera.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('H', '2', '6', '4'));
    while(True):
        success, frame = camera.read()
        ret, buffer = cv2.imencode('.jpg', frame,(cv2.IMWRITE_JPEG_QUALITY, 90))
        producer.send(topic, buffer.tobytes())
            
        # Choppier stream, reduced load on processor
        time.sleep(0.2)


if __name__ == '__main__':
    print("publishing feed!")
    publish_camera()

