# Deployment of Intel Edge Insights for Industrial to MicroShift
Intel Edge Insights for Industrialsは、Intelの開発した産業向け画像解析システムを簡単に実装するためのオープンソースのフレームワークです。詳しくは、[Qiita](https://qiita.com/ydo/items/81c93f53467c44413600)を参照してください。

本リポジトリのコンテナレジストリに、コンテナイメージへビルド済みのイメージもあります。


